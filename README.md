# FederateMe

A site to help people get started on the Fediverse

This site helps peope find sites on the Fediverse that they can use. It will lead them to the service's home page, where they can find out more and find a server to join!

If you have a favorite service that you use on the Fediverse, and don't see it on this list, add a comment with the site name, a short description and the home page url (the one where you can learn more, find a server and sign up) and I will try to add it as soon as I can! Please note that this is for the services themselves, not a specific instance or server. (e.g Mastodon rather than a specific instance like Fosstodon.)

